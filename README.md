Introduction
------
This is a Chinese corpus containing sentences annotated with verbal multiword expressions (VMWEs), following the PARSEME guideline, edition 1.3. 
See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Chinese data result from an update and an extension of the Chinese part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367).
For the changes with respect to the 1.2 version, see the change log below.

The raw corpus released in version 1.2 can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Source
-------
The annotated data comes from [Universal Dependencies](https://universaldependencies.org/) and [CoNLL 2017 Shared Task](http://universaldependencies.org/conll17/), where _Crawl-000_ and _Crawl-001_ consist of text from web pages and _Wiki-000_ consists of text from wikipedia Traditional Chinese. The detailed information is in the following:

|Corpus|Source|Sentences|Tokens|
|------|------|---------|------|
|GSD-dev|UD Chinese GSD v2.8|500|12663|
|GSD-test|UD Chinese GSD v2.8|500|12012|
|GSD-train|UD Chinese GSD v2.8|3997|98616|
|HK-test|UD Chinese v2.1|1004|9874|
|PUD-test|UD Chinese v2.8|1000|21415|
|Crawl-000|CoNLL 2017 Shared Task|30274|445531|
|Crawl-001|CoNLL 2017 Shared Task|9000|170771|
|Wiki-000|CoNLL 2017 Shared Task|2654|49465|
|TOTAL| | 48929 | 820347 |

Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. The following tagsets are used:
* column 4 (UPOS): For the GSD/HK/PUD arts: synchronised with the (mostly manual) annotations in the UD Chinese GSD (v2.8), HK (v2.1) and PUD (v2.8) treebanks. For the for Crawl-000 and Wiki-000 parts: automatically annotated with UDPipe 2 (`ZH-crawl-wiki-udpipe.2.10.cupt` model). The tagset is  [UD POS-tags](http://universaldependencies.org/u/pos). 
* column 5 (XPOS): For the GSD/HK/PUD arts: synchronised with the (mostly manual) annotations in the UD Chinese GSD (v2.8), HK (v2.1) and PUD (v2.8) treebanks. For the for Crawl-000 and Wiki-000 parts: automatically annotated with UDPipe 2 (`ZH-crawl-wiki-udpipe.2.10.cupt` model).
* column 6 (FEATS): For the GSD/HK/PUD arts: synchronised with the (mostly manual) annotations in the UD Chinese GSD (v2.8), HK (v2.1) and PUD (v2.8) treebanks. For the for Crawl-000 and Wiki-000 parts: automatically annotated with UDPipe 2 (`ZH-crawl-wiki-udpipe.2.10.cupt` model). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html). For the GSD/HK/PUD arts: synchronised with the (mostly manual) annotations in the UD Chinese GSD (v2.8), HK (v2.1) and PUD (v2.8) treebanks. For the for Crawl-000 and Wiki-000 parts: automatically annotated with UDPipe 2 (`ZH-crawl-wiki-udpipe.2.10.cupt` model). 
* columns 7-8 (HEAND and DEPREL): For the GSD/HK/PUD arts: synchronised with the (mostly manual) annotations in the UD Chinese GSD (v2.8), HK (v2.1) and PUD (v2.8) treebanks. For the for Crawl-000 and Wiki-000 parts: automatically annotated with UDPipe 2 (`ZH-crawl-wiki-udpipe.2.10.cupt` model). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* column 11 (PARSEME:MWE): Manually annotated using the [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) version 1.3.


Tokenization
-------------
The UD  Chinese (GSD/HK/PUD) part is the manually annotated from UD Chinese guideline. The CoNLL 2017 Shared Task (Crawl-000 and Wiki-000) part was initially tokenized for release 1.2 using the UDPipe tool v2.5, except that some of the segmentation errors have been corrected if the segmentation errors have affected the annotation of the right scope of a VMWE. For example, _處理 得乾 淨_ will be corrected as _處理 得 乾淨_, and then _處理_ and _乾淨_ will be annotated as VPC.semi. 
For release 1.3 the same tokenization was maintained, including the Crawl-001 sentences, added in this edition.


Annotations
--------------------
To be indenpendent to the segmentation issues in Chinese that sometimes a string can be either one or two words depending on the principle, we annotate both single-token VMWEs (but certainly multiple morphemes) and multi-token VMWEs. 


Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

<!--
**[TODO: update the statistics]**

|VMWE Type|Single Token|Double Token|Multi-Token|
|---------|-----------|-----------|----------|
|LVC.cause|0|171|6|
|LVC.full|0|1156|58|
|MVC|1591|2220|15|
|VID|775|83|115|
|VPC.semi|3016|1594|19|
-->


Licence
-------
The full dataset is licensed under Creative Commons Non-Commercial Share-Alike 4.0 license CC-BY-NC-SA 4.0.


Authors
-------
Hongzhi Xu, Menghan Jiang


Contributors
-------
Jia Chen, Xiaomin Ge, Fangyuan Hu, Sha Hu, Minli Li, Siyuan Liu, Zhenzhen Qin, Ruilong Sun, Chengwen Wang, Huangyang Xiao, Peiyi Yan, Tsy Yih, Ke Yu, Songping Yu, Si Zeng, Yongchen Zhang, Yun Zhao


Raw corpus
-------
The raw corpus contains 4 files:
 * `raw-001.conllu`
    * origin: `conll2017-crawl-000`
    * Sentence count: 348935
    * Token count: 5150812
 * `raw-002.conllu`
    * origin: `conll2017-crawl-001`
    * Sentence count: 1175265
    * Token count: 18450549
 * `raw-003.conllu`
    * origin: `conll2017-crawl-002`
    * Sentence count: 1144700
    * Token count: 18588146
 * `raw-004.conllu`
    * origin: `conll2017-wiki-000`
    * Sentence count: 1438279
    * Token count: 25046404

Contact
-------
 * Hongzhi Xu: hxu@shisu.edu.cn
 * Menghan Jiang: menghanengl.jiang@polyu.edu.hk

Future work
-------
 * The not-to-release/2023-updates/annotation_2022/ZH-all-2-plus2&8.cupt file contains new annotations that call for extra verifications and format enhancements. They should be made ready for the next release.
 * It would be good to double-check the morphosyntactic annotations in the HK fraction of the corpus. It was synchronised with the UD release 2.11. But inside this release, the HK sub-corpus seems to be in version 1.51 (although first released with v2.1 of UD). SInce a mojor change in UD tagsets happened in version 2.0, it should be checked that the tagsets used in HK are truly up to date with UD 2.

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
    - New 9000 sentences from Crawl-001 were manually annotated for VMWEs. The morphosyntactic annotations (columns LEMMA to DEPREL) were re-generated with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2/) (chinese-gsd-ud-2.10-220711 model) by Agata Savary.
    - The morphosyntactic annotation (columns LEMMA to DEPREL) in GSD, HK and PUD was synchronised with UD release 2.11.
    - The morphosyntactic annotation (columns LEMMA to DEPREL) in Crawl-000 and Wiki-000 was re-generated with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2/) (chinese-gsd-ud-2.10-220711 model) by Agata Savary.
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.

